<?php
// $Id$
/*
 * @file uc_rentals.views.inc
 * Provides views integration for uc_rentals
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Implementation of hook_views_data().
 */
function uc_rentals_views_data() {
  $data['uc_rentals_rentals']['table']['group'] = t('UC Rentals');
  $data['uc_rentals_rentals']['table']['join']['uc_order_products'] = array(
    'left_field' => 'order_product_id',
    'field' => 'order_product_id',
  );

  $data['uc_rentals_rentals']['start_dt'] = array(
    'title' => t('Rental start date'),
    'help' => t('The start date of the rental.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    )
  );
  
  $data['uc_rentals_rentals']['end_dt'] = array(
    'title' => t('Rental end date'),
    'help' => t('The end date of the rental.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    )
  );
  
  return $data;
}