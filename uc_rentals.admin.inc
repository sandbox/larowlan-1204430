<?php
/*
 * @file uc_rentals.admin.inc
 * Provides admin settings form for uc_rentals
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */
/**
 * Provides
*/
function uc_rentals_admin_form($form_state) {
  $form = array();
  
  $roles = user_roles(TRUE, 'Rent items');
  
  $form['roles'] = array(
    '#tree' => TRUE,
    '#theme' => 'uc_rentals_admin_form_table'
  );
  
  foreach ($roles as $rid => $name) {
    $settings = uc_rentals_role_settings($rid);
    if (!$settings) {
      $settings = new stdClass();
      $settings->max_length = 
        $settings->max_per_month =
        $settings->max_concurrent = 1;
    }
    $form['roles'][$rid]['role'] = array(
      '#type' => 'value',
      '#value' => array('rid' => $rid, 'name' => $name)
    );
    $form['roles'][$rid]['title'] = array(
      '#value' => $name
    );
    $form['roles'][$rid]['max_length'] = array(
      '#type' => 'select',
      '#default_value' => $settings->max_length,
      '#options' => drupal_map_assoc(range(1,24)),
    );
    $form['roles'][$rid]['max_per_month'] = array(
      '#type' => 'select',
      '#default_value' => $settings->max_per_month,
      '#options' => drupal_map_assoc(range(1,24)),
    );
    $form['roles'][$rid]['max_concurrent'] = array(
      '#type' => 'select',
      '#default_value' => $settings->max_concurrent,
      '#options' => drupal_map_assoc(range(1,24)),
    );
    
  }
  
  $form['transit_days'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Transit days'),
    '#default_value' => variable_get('uc_rentals_transit_days', 2),
    '#size'          => 4,
    '#description'   => t('Enter number of days to add to end of availability to allow for return transport'),
    '#maxlength'     => 2,
    '#required'      => TRUE,
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  
  return $form;
}

/**
 * Submit handler
*/
function uc_rentals_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values['roles'] as $rid => $role) {
    $original = uc_rentals_role_settings($rid);
    $role['rid'] = $rid;
    $update = array();
    if ($original) {
      $update = array('rid');      
    }
    drupal_write_record('uc_rentals_roles', $role, $update);
  }
  variable_set('uc_rentals_transit_days', $values['transit_days']);
  drupal_set_message(t('Saved changes'));
}

